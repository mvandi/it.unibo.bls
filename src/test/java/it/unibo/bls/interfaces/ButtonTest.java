package it.unibo.bls.interfaces;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import org.junit.Test;

import it.unibo.bls.interfaces.IObserver;

public class ButtonTest extends TestConfigurator {

	private IObserver observer;

	@Test
	public void testCreation() {
		System.out.println("[button]: Test creation");

		verify(observer, never()).update(anyString());
	}

	@Test
	public void testPress() {
		System.out.println("[button]: Test press");

		mockButton.press();
		verify(observer).update(eq("pressed"));
	}

	@Override
	protected void onBuild() {
		super.onBuild();
		observer = mock(IObserver.class);
		mockButton.addObserver(observer);
	}

}
