package it.unibo.bls.interfaces;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/*
 * Integration test
 */
public class ControlTest extends TestConfigurator {

	@Test
	public void testCreation() {
		System.out.println("[control]: Test creation");

		assertFalse(ledModel.isOn());
	}

	@Test
	public void testPressWhenOff() {
		System.out.println("[contro]: Test press button when led is off");

		mockButton.press();
		assertTrue(ledModel.isOn());
	}

	@Test
	public void testPressWhenOn() {
		System.out.println("[contro]: Test press button when led is on");

		ledModel.turnOn();

		mockButton.press();
		assertFalse(ledModel.isOn());
	}

}
