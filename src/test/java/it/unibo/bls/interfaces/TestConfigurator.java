package it.unibo.bls.interfaces;

import org.junit.After;
import org.junit.Before;

import it.unibo.bls.implementations.MockButton;
import it.unibo.bls.main.AbstractConfigurator;

public class TestConfigurator extends AbstractConfigurator {

	protected MockButton mockButton;

	@Before
	public void setUp() {
		build();
	}

	@After
	public void tearDown() {
		dismiss();
	}

	@Override
	protected void onBuild() {
		mockButton = new MockButton();
		mockButton.addObserver(buttonModel);
	}

	@Override
	protected void onDismiss() {
		mockButton.clearObservers();
	}

}
