package it.unibo.bls.interfaces;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/*
 * Unit test
 */
public class LedTest extends TestConfigurator {

	@Test
	public void testCreation() {
		System.out.println("[led]: Test creation");

		assertFalse(ledModel.isOn());
	}

	@Test
	public void testTurnOn() {
		System.out.println("[led]: Test turn on");

		ledModel.turnOn();
		assertTrue(ledModel.isOn());
	}

	@Test
	public void testTurnOff() {
		System.out.println("[led]: Test turn off");

		ledModel.turnOn();
		assertTrue(ledModel.isOn());

		ledModel.turnOff();
		assertFalse(ledModel.isOn());
	}

}
