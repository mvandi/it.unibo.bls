package it.unibo.bls.main;

import it.unibo.bls.interfaces.IButtonModel;
import it.unibo.bls.interfaces.ILedModel;
import it.unibo.bls.interfaces.IObserver;
import it.unibo.bls.model.BusinessLogic;
import it.unibo.bls.model.ButtonModel;
import it.unibo.bls.model.LedModel;

public abstract class AbstractConfigurator {

	private IObserver control;
	protected ILedModel ledModel;
	protected IButtonModel buttonModel;

	protected AbstractConfigurator() {
	}

	public final void build() {
		createComponents();
		onBuild();
		startComponents();
	}

	public final void dismiss() {
		ledModel.clearObservers();
		buttonModel.clearObservers();

		onDismiss();

		ledModel = null;
		control = null;
		buttonModel = null;
	}

	private void createComponents() {
		ledModel = LedModel.create();
		control = BusinessLogic.create(ledModel);
		buttonModel = ButtonModel.create();

		buttonModel.addObserver(control);
	}

	protected void onBuild() {
	}

	protected void onDismiss() {
	}

	protected void startComponents() {
	}

}
