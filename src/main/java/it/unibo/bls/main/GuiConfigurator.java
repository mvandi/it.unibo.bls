package it.unibo.bls.main;

import it.unibo.bls.implementations.GuiButton;
import it.unibo.bls.implementations.GuiLed;

public final class GuiConfigurator extends AbstractConfigurator {

	private GuiLed guiLed;

	private GuiButton guiButton;

	public static void main(final String... args) {
		new GuiConfigurator().build();
	}

	protected void onBuild() {
		guiLed = new GuiLed();
		ledModel.addObserver(guiLed);
		guiButton = new GuiButton();
		guiButton.addObserver(buttonModel);
	}

	protected void onDismiss() {
		guiButton.clearObservers();
	}

	private GuiConfigurator() {
	}

}
