package it.unibo.bls.implementations;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;

import it.unibo.bls.interfaces.IObserver;

public class GuiLed implements IObserver {

	private final JPanel panel;
	
	public GuiLed() {
		final JFrame frame = new JFrame("Led GUI");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(300, 300);
		panel = new JPanel();
		panel.setOpaque(true);
		panel.setBackground(Color.WHITE);
		frame.add(panel);
		frame.setVisible(true);
	}

	@Override
	public void update(String state) {
		if ("on".equals(state)) {
			panel.setBackground(Color.GREEN);
		} else if ("off".equals(state)) {
			panel.setBackground(Color.WHITE);
		}
	}

}
