package it.unibo.bls.implementations;

import javax.swing.JButton;
import javax.swing.JFrame;

import it.unibo.bls.interfaces.AbstractObservable;

public class GuiButton extends AbstractObservable {

	public GuiButton() {
		final JFrame frame = new JFrame("Button GUI");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(300, 300);
		final JButton button = new JButton("");
		frame.getContentPane().add(button);
		button.addActionListener(e -> notify("pressed"));
		frame.setVisible(true);
	}

}
