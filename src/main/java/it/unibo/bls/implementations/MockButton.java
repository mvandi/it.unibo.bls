package it.unibo.bls.implementations;

import it.unibo.bls.interfaces.AbstractObservable;
import it.unibo.bls.interfaces.IMockButton;

public final class MockButton extends AbstractObservable implements IMockButton {
	
	public MockButton() {
		System.out.println("Mock button created");
	}

	@Override
	public void press() {
		System.out.println("Mock button pressed");
		notify("pressed");
	}

}
