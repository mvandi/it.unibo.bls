package it.unibo.bls.interfaces;

public interface IObserver {

	void update(String state);

}
