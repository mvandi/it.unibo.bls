package it.unibo.bls.interfaces;

import java.util.Collection;
import java.util.LinkedList;
import java.util.function.Supplier;

public abstract class AbstractObservable implements IObservable {

	private final Collection<IObserver> observers;

	protected AbstractObservable() {
		this(LinkedList::new);
	}

	protected AbstractObservable(final Supplier<? extends Collection<IObserver>> collectionSuppplier) {
		observers = collectionSuppplier.get();
	}

	@Override
	public final void addObserver(final IObserver observer) {
		observers.add(observer);
	}

	@Override
	public final void removeObserver(final IObserver observer) {
		observers.remove(observer);
	}

	@Override
	public final void clearObservers() {
		observers.clear();
	}

	protected final synchronized void notify(final String state) {
		observers.forEach(observer -> observer.update(state));
	}

}
