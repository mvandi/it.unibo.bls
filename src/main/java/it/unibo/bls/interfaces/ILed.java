package it.unibo.bls.interfaces;

public interface ILed {

	boolean isOn();

	void turnOn();

	void turnOff();

	void ledSwitch();

}
