package it.unibo.bls.interfaces;

public interface IMockButton extends IObservable {

	void press();

}
