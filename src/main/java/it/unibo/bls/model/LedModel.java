package it.unibo.bls.model;

import it.unibo.bls.interfaces.AbstractObservable;
import it.unibo.bls.interfaces.ILedModel;

public final class LedModel extends AbstractObservable implements ILedModel {

	public static ILedModel create() {
		return new LedModel();
	}

	private boolean on;

	private LedModel() {
		on = false;
	}

	@Override
	public boolean isOn() {
		return on;
	}

	@Override
	public void turnOn() {
		on = true;
		notify("on");
	}

	@Override
	public void turnOff() {
		on = false;
		notify("off");
	}

	@Override
	public void ledSwitch() {
		if (on) {
			turnOff();
		} else {
			turnOn();
		}
	}

}
