package it.unibo.bls.model;

import it.unibo.bls.interfaces.AbstractObservable;
import it.unibo.bls.interfaces.IButtonModel;

public final class ButtonModel extends AbstractObservable implements IButtonModel {

	public static IButtonModel create() {
		return new ButtonModel();
	}

	private ButtonModel() {
	}

	@Override
	public void update(final String state) {
		notify(state);
	}

}
