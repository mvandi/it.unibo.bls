package it.unibo.bls.model;

import it.unibo.bls.interfaces.ILed;
import it.unibo.bls.interfaces.IObserver;

public final class BusinessLogic implements IObserver {

	public static IObserver create(ILed led) {
		return new BusinessLogic(led);
	}

	private final ILed led;

	private BusinessLogic(final ILed led) {
		this.led = led;
	}

	@Override
	public void update(final String state) {
		if ("pressed".equals(state)) {
			led.ledSwitch();
		}
	}

}
